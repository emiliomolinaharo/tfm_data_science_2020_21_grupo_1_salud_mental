# TFM - Data Science - Emilio Molina

Estudio que recogerá los índices de incidencia sobre salud mental en la Unión Europea y su relación con datos socioeconómicos, de consumo y medioambientales de cada país. El objetivo es identificar qué factores pueden ser considerados como variables significativas e influyentes en el modelo para establecer un pronóstico en función de la evolución de las mismas. Esto podría ayudar a establecer planes de acción sobre estas variables, que pueden llegar a ser controlables, para prevenir y tratar la ansiedad, la depresión y la distimia.

**Estructura de carpetas:**

* **raw datasets:** Contiene las extracciones de datos sin tratar procedentes de cada fuente.

* **memoria:** Documentos entregables del trabajo.

* Cuaderno **Salud Mental.ipynb:** Cuaderno de Jupyter sobre el que se han desarrollado las tareas de exploración, limpieza y construcción del modelo.

Histórico:

| Action | Date | Comment |
| ------ | ------ | ------ |
| Setup del Gitlab | 22/09/2021 | Configuración del sitio |
| Carga de datasets crudos | 09/10/2021 | Datasets obtenidos de diferentes fuentes y con diferentes formatos |
| Resumen de datasets crudos | 16/11/2021 | Análisis de datasets, variables, missing data, formato y unidades de medida. Disponible [aquí](Variables summary.md). |
| Limpieza de datos / construcción de dataset principal | 25/11/2021 - 22/12/2021 | |
| Confección de la memoria en Word | 01/12/2021 - 16/01/2022 | |
| Data cleansing en Python (missing data, incomplete time series, etc.) | 23/12/2021 - 06/01/2022 | |
| Visualizaciones en Tableau | 28/12/2021 - 31/31/2021 | |
| Aplicación de Machine Learning | 06/01/2021 - 09/01/2022| Selección de modelo, feature tuning, y visualización de modelos |

