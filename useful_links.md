1. [Data Bank (series temporales sobre datos generales de países)](https://databank.worldbank.org/databases/page/1/orderby/popularity/direction/desc?qterm=mental)
2. [Global Health Data Exchange (banco de datos sobre salud)](http://ghdx.healthdata.org/gbd-results-tool)
3. [Special Eurobarometer 345 - Mental Health](https://data.europa.eu/data/datasets/s898_73_2_ebs345?locale=en)
4. [Pay up or put it off: how Europe treats depression and anxiety](https://civio.es/medicamentalia/2021/03/09/access-to-mental-health-in-europe/)


Working with Time series:
- [Missing values in Time Series in python](https://stackoverflow.com/questions/49308530/missing-values-in-time-series-in-python) (compara fillna vs. interpolate)
- [Imputing the Time-Series Using Python](https://drnesr.medium.com/filling-gaps-of-a-time-series-using-python-d4bfddd8c460)